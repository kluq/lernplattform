<?php
/**
 * Grundeinstellungen für WordPress
 *
 * Zu diesen Einstellungen gehören:
 *
 * * MySQL-Zugangsdaten,
 * * Tabellenpräfix,
 * * Sicherheitsschlüssel
 * * und ABSPATH.
 *
 * Mehr Informationen zur wp-config.php gibt es auf der
 * {@link https://codex.wordpress.org/Editing_wp-config.php wp-config.php editieren}
 * Seite im Codex. Die Zugangsdaten für die MySQL-Datenbank
 * bekommst du von deinem Webhoster.
 *
 * Diese Datei wird zur Erstellung der wp-config.php verwendet.
 * Du musst aber dafür nicht das Installationsskript verwenden.
 * Stattdessen kannst du auch diese Datei als wp-config.php mit
 * deinen Zugangsdaten für die Datenbank abspeichern.
 *
 * @package WordPress
 */

// ** MySQL-Einstellungen ** //
/**   Diese Zugangsdaten bekommst du von deinem Webhoster. **/

/**
 * Ersetze datenbankname_hier_einfuegen
 * mit dem Namen der Datenbank, die du verwenden möchtest.
 */
define( 'DB_NAME', 'wordpress' );

/**
 * Ersetze benutzername_hier_einfuegen
 * mit deinem MySQL-Datenbank-Benutzernamen.
 */
define( 'DB_USER', 'root' );

/**
 * Ersetze passwort_hier_einfuegen mit deinem MySQL-Passwort.
 */
define( 'DB_PASSWORD', 'root' );

/**
 * Ersetze localhost mit der MySQL-Serveradresse.
 */
define( 'DB_HOST', 'localhost' );

/**
 * Der Datenbankzeichensatz, der beim Erstellen der
 * Datenbanktabellen verwendet werden soll
 */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Der Collate-Type sollte nicht geändert werden.
 */
define('DB_COLLATE', '');

/**
 * WP UPLOAD FILESIZE 1000M
 */
define('WP_MEMORY_LIMIT', '1000M');

/**#@+
 * Sicherheitsschlüssel
 *
 * Ändere jeden untenstehenden Platzhaltertext in eine beliebige,
 * möglichst einmalig genutzte Zeichenkette.
 * Auf der Seite {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * kannst du dir alle Schlüssel generieren lassen.
 * Du kannst die Schlüssel jederzeit wieder ändern, alle angemeldeten
 * Benutzer müssen sich danach erneut anmelden.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '[.E(_s?{ZG?L<yE=:?wE#{ldLLxs[VDfXgW6fpbaoXr^&;a(!ETqR}<xj(YUhm2/' );
define( 'SECURE_AUTH_KEY',  's*9[c&Z]/5J>G*)Lt_}WAs5@^qKG8AYteNdJlwSA1lk7p8;2]9DBpL4$?1z)uq$4' );
define( 'LOGGED_IN_KEY',    'QNS>/?d.2B.O%^EFZ$lV#BK^qGUxhkDVBbbG@4GBG^ofH-q$h #ZGQ+Q!WGcIdGi' );
define( 'NONCE_KEY',        '[.DQ&Wq1EW`GvZD2yniQq[k>kFk9^~!F^t(1)<_P$QqkZ{qn~Xgg9g,&OpJ<Qo*X' );
define( 'AUTH_SALT',        'g&T4$NQ%mbXGk!p=O3LH`F1+:R@:*GC|/v>vLprp62QogOJkLU0?o2jYkj+%Ha1w' );
define( 'SECURE_AUTH_SALT', 'oCW]69$^1ZQ~oskG`[_Px$gGvnX6OkRl`HlF:deOk0nz{~>/zkxV1u#l7k:)p_=M' );
define( 'LOGGED_IN_SALT',   '1{@tj`j1sBiRW6+%%0VimJdtU{vL-A6yH(?3A$YCfwZq}|o#9FKlAwzA,kFnY>@[' );
define( 'NONCE_SALT',       '^7{ *b{yZ+.G[Dm.C<./2-Avzg`K/4t}]]}zla8,oZjqJgB*/|Q$$?qf(Ioo:N[%' );

/**#@-*/

/**
 * WordPress Datenbanktabellen-Präfix
 *
 * Wenn du verschiedene Präfixe benutzt, kannst du innerhalb einer Datenbank
 * verschiedene WordPress-Installationen betreiben.
 * Bitte verwende nur Zahlen, Buchstaben und Unterstriche!
 */
$table_prefix = 'wp_';

/**
 * Für Entwickler: Der WordPress-Debug-Modus.
 *
 * Setze den Wert auf „true“, um bei der Entwicklung Warnungen und Fehler-Meldungen angezeigt zu bekommen.
 * Plugin- und Theme-Entwicklern wird nachdrücklich empfohlen, WP_DEBUG
 * in ihrer Entwicklungsumgebung zu verwenden.
 *
 * Besuche den Codex, um mehr Informationen über andere Konstanten zu finden,
 * die zum Debuggen genutzt werden können.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Das war’s, Schluss mit dem Bearbeiten! Viel Spaß. */
/* That's all, stop editing! Happy publishing. */

/** Der absolute Pfad zum WordPress-Verzeichnis. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Definiert WordPress-Variablen und fügt Dateien ein.  */
require_once( ABSPATH . 'wp-settings.php' );
